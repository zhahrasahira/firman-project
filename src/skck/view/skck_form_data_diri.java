/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package skck.view;

/**
 *
 * @author gu
 */
public class skck_form_data_diri extends javax.swing.JFrame {

    /**
     * Creates new form skck_form_daftar_satwil
     */
    public skck_form_data_diri() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jProgressBar1 = new javax.swing.JProgressBar();
        btnsatwil = new javax.swing.JButton();
        btndata = new javax.swing.JButton();
        btnhubkel = new javax.swing.JButton();
        btnpend = new javax.swing.JButton();
        btnperpid = new javax.swing.JButton();
        btncirfis = new javax.swing.JButton();
        btnlamp = new javax.swing.JButton();
        btnket = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        btnBatal = new javax.swing.JButton();
        btnKembali = new javax.swing.JButton();
        btnLanjut = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel1.setText("Form. Pendaftaran");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));
        getContentPane().add(jProgressBar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 880, 20));

        btnsatwil.setText("Satwil");
        btnsatwil.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnsatwil.setEnabled(false);
        btnsatwil.setFocusPainted(false);
        btnsatwil.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnsatwilActionPerformed(evt);
            }
        });
        getContentPane().add(btnsatwil, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 110, -1));

        btndata.setText("Data Pribadi");
        btndata.setAutoscrolls(true);
        btndata.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                btndataFocusGained(evt);
            }
        });
        btndata.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btndataActionPerformed(evt);
            }
        });
        getContentPane().add(btndata, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 40, 110, -1));

        btnhubkel.setText("Hub. Keluarga");
        btnhubkel.setEnabled(false);
        btnhubkel.setFocusPainted(false);
        btnhubkel.setFocusable(false);
        btnhubkel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnhubkelActionPerformed(evt);
            }
        });
        getContentPane().add(btnhubkel, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 40, 110, -1));

        btnpend.setText("Pendidikan");
        btnpend.setEnabled(false);
        btnpend.setFocusPainted(false);
        btnpend.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpendActionPerformed(evt);
            }
        });
        getContentPane().add(btnpend, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 40, 110, -1));

        btnperpid.setText("Perkara Pidana");
        btnperpid.setEnabled(false);
        btnperpid.setFocusPainted(false);
        btnperpid.setFocusable(false);
        btnperpid.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnperpidActionPerformed(evt);
            }
        });
        getContentPane().add(btnperpid, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 40, 110, -1));

        btncirfis.setText("Ciri Fisik");
        btncirfis.setEnabled(false);
        btncirfis.setFocusPainted(false);
        btncirfis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncirfisActionPerformed(evt);
            }
        });
        getContentPane().add(btncirfis, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 40, 110, -1));

        btnlamp.setText("Lampiran");
        btnlamp.setEnabled(false);
        btnlamp.setFocusPainted(false);
        btnlamp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnlampActionPerformed(evt);
            }
        });
        getContentPane().add(btnlamp, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 40, 110, -1));

        btnket.setText("Keterangan");
        btnket.setEnabled(false);
        btnket.setFocusPainted(false);
        btnket.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnketActionPerformed(evt);
            }
        });
        getContentPane().add(btnket, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 40, 110, -1));

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 880, 390));

        btnBatal.setBackground(java.awt.Color.red);
        btnBatal.setText("Batalkan");
        btnBatal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBatalActionPerformed(evt);
            }
        });
        getContentPane().add(btnBatal, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 510, -1, -1));

        btnKembali.setText("Kembali");
        btnKembali.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnKembaliActionPerformed(evt);
            }
        });
        getContentPane().add(btnKembali, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 510, -1, -1));

        btnLanjut.setText("Lanjut");
        btnLanjut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLanjutActionPerformed(evt);
            }
        });
        getContentPane().add(btnLanjut, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 510, -1, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnhubkelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnhubkelActionPerformed
        
    }//GEN-LAST:event_btnhubkelActionPerformed

    private void btnBatalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBatalActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnBatalActionPerformed

    private void btnsatwilActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnsatwilActionPerformed
        
    }//GEN-LAST:event_btnsatwilActionPerformed

    private void btnKembaliActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnKembaliActionPerformed
        this.dispose();
        skck_form_satwil obj = new skck_form_satwil();
        obj.setVisible(true);
    }//GEN-LAST:event_btnKembaliActionPerformed

    private void btndataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btndataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btndataActionPerformed

    private void btnperpidActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnperpidActionPerformed
        
    }//GEN-LAST:event_btnperpidActionPerformed

    private void btnpendActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpendActionPerformed
        
    }//GEN-LAST:event_btnpendActionPerformed

    private void btncirfisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncirfisActionPerformed
       
    }//GEN-LAST:event_btncirfisActionPerformed

    private void btnlampActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnlampActionPerformed
        
    }//GEN-LAST:event_btnlampActionPerformed

    private void btnketActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnketActionPerformed
        
    }//GEN-LAST:event_btnketActionPerformed

    private void btnLanjutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLanjutActionPerformed
        this.dispose();
        skck_form_hubkeluarga obj = new skck_form_hubkeluarga();
        obj.setVisible(true);
    }//GEN-LAST:event_btnLanjutActionPerformed

    private void btndataFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_btndataFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_btndataFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(skck_form_data_diri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(skck_form_data_diri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(skck_form_data_diri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(skck_form_data_diri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new skck_form_data_diri().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBatal;
    private javax.swing.JButton btnKembali;
    private javax.swing.JButton btnLanjut;
    private javax.swing.JButton btncirfis;
    private javax.swing.JButton btndata;
    private javax.swing.JButton btnhubkel;
    private javax.swing.JButton btnket;
    private javax.swing.JButton btnlamp;
    private javax.swing.JButton btnpend;
    private javax.swing.JButton btnperpid;
    private javax.swing.JButton btnsatwil;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JProgressBar jProgressBar1;
    // End of variables declaration//GEN-END:variables
}
